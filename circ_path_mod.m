% p_i : punto iniziale della traiettoria
% c : centro della circonferenza
% angle : angolo da compiere (rad)
% a_max : accelerazione massima
% t : tempo in cui eseguire la traiettoria
% R : matrice di rotazione dell'asse della circonferenza
% via : 1 = punto di via, 0 = non punto di via

%function [p, dot_p, dot_dot_p] = circ_path_mod(p_i,c,angle,a_max,t,axis,via)
function [p, dot_p, dot_dot_p] = circ_path_mod(param_rapp, p_i, c, rho, a_max, axis)

    s = param_rapp.s;
    sp = param_rapp.ds;
    spp = param_rapp.dds;
    
    
    x1=(p_i - c)'/norm(p_i-c);
    z1=axis'/norm(axis)';
    y1=cross(z1,x1);
    R=[x1 y1 z1];
    
    for i=1:length(s)
        
        pp = [rho*cos(s(i)/rho); rho*sin(s(i)/rho); 0];
        
        p(i,1) = c(1) + R(1,:)*pp;
        p(i,2) = c(2) + R(2,:)*pp;
        p(i,3) = c(3) + R(3,:)*pp;
        %p(i,:)=c'+R*pp;
        
        dot_p(i,1) = R(1,:)*[-sp(i)*sin(s(i)/rho); sp(i)*cos(s(i)/rho); 0];
        dot_p(i,2) = R(2,:)*[-sp(i)*sin(s(i)/rho); sp(i)*cos(s(i)/rho); 0];
        dot_p(i,3) = R(3,:)*[-sp(i)*sin(s(i)/rho); sp(i)*cos(s(i)/rho); 0];

        
        dot_dot_p(i,1) = R(1,:)*[-(sp(i)^2)*cos(s(i)/rho)/rho - spp(i)*sin(s(i)/rho);...
                                 -(sp(i)^2)*sin(s(i)/rho)/rho - spp(i)*cos(s(i)/rho);...
                                 0];
        dot_dot_p(i,2) = R(2,:)*[-(sp(i)^2)*cos(s(i)/rho)/rho - spp(i)*sin(s(i)/rho);...
                                 -(sp(i)^2)*sin(s(i)/rho)/rho - spp(i)*cos(s(i)/rho);...
                                 0];
        dot_dot_p(i,3) = R(3,:)*[-(sp(i)^2)*cos(s(i)/rho)/rho - spp(i)*sin(s(i)/rho);...
                                 -(sp(i)^2)*sin(s(i)/rho)/rho - spp(i)*cos(s(i)/rho);...
                                 0];
    end
    
    


end