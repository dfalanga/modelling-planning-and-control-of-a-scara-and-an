function [s,sp,spp,D_t]=vel_trap_mod(sf,si,Ts,ti,tf,t_tot,via,a_max,Dt_tot)


%controllo sull'accelerazione
a_min=(4*abs(sf-si)/((tf-ti)^2));
while a_max<a_min
    s1=s('Valore di accelerazione non sufficiente.\nInserisci un valore di accelerazione pari a %d o maggiore.', a_min);
    disp(s1);
    a_max=input('-->');
end
%------------------



%La lunghezza dell'arco ? zero dal tempo iniziale t=0 al tempo in cui
%inizia il segmento t=ti
%calcolo tc
tc=0.5*(tf-ti)-0.5*sqrt((1/a_max)*(((tf-ti)^2)*a_max-4*sf+4*si));

%Calcolo_delta_t del blend (faccio l'arrotondamento per evitare errori nei
%passi successivi)
D_t=chop(tc*2,2);
if via == 1
    Dt=D_t+Dt_tot; %Considero anche i delta_t dei punti di via precedenti
else
    Dt=Dt_tot; %Considero i delta_t dei punti di via precedenti
end

%La lunghezza dell'arco ? zero dal tempo iniziale t=0 al tempo in cui
s_0= zeros(size(0:Ts:ti-Dt));

%calcolo profilo trapezoidale nell'intervallo tf-ti
t=0:Ts:(tf-ti);
for i=1:length(t)
    if i<=(tc/Ts)
        s_j(i)=0+0.5*a_max*t(i)^2;
        s_j_p(i)=a_max*t(i);
        s_j_pp(i)=a_max;
    elseif i>=((tf-ti)-tc)/Ts
            s_j(i)=sf-0.5*a_max*((tf-ti)-t(i))^2;
            s_j_p(i)=a_max*((tf-ti)-t(i));
            s_j_pp(i)=a_max;
    else
            s_j(i)=0+a_max*tc*(t(i)-0.5*tc);
            s_j_p(i)=a_max*tc;
            s_j_pp(i)=0;
    end
end

%La lunghezza dell'arco ? sf dal tempo t=tf al tempo in cui
%finisce la traiettoria t=t_tot
t=(tf-Dt):Ts:t_tot;
s_f = sf*ones(size(t));

%Concateno le stringhe
s = [s_0 s_j s_f];
sp = [s_0 s_j_p 0*s_f];
spp = [s_0 s_j_pp 0*s_f];
return
            

