%% Matlab implementation of CLIK algorithm
%  

function [q, dot_q, dot_dot_q, Xe] = click_algorithm(clik_type, p, dot_p, dot_dot_p, orientation, q0,  t_sam) 
%% Integration parameters (Euler integration)
t_i   = 0;                % Initial instant
t_f   = length(p)*t_sam-t_sam;
int   = (t_i:t_sam:t_f)'; % Integration interval

%% Desired trajectory in the operative space
X_d(:,1:3) = p(:,1:3); % Position
X_d(:,4) = orientation; % Orientation

dot_X_d(:,1:3) = dot_p(:,1:3);
dot_X_d(:,4) = zeros(1,length(dot_p));

dot_dot_X_d(:,1:3) = dot_dot_p(:,1:3);
dot_dot_X_d(:,4) = zeros(1,length(dot_dot_p));

%% Gains
if clik_type == 1
    Kp = [500 500 500 500]; % Proportional gain (x y z phi)
elseif clik_type == 2
    Kp = [500 500 500 500]; % Proportional gain (x y z phi)
else
    Kp = [10000 10000 1000 500]; % Proportional gain (x y z phi)
    Kd = [500 500 500 250];  % Derivative gain
end
%% Initial conditions for q, dot_q, dot_dot_q, Xe and dot_Xe vectors
q(1,:) = q0;
dot_q(1,:) = [0 0 0 0];
dot_dot_q(1,:) = [0 0 0 0];

Xe(1,:) = direct_kinematics(q(1,:));
dot_Xe(1,:) = jacobian(q(1,:))*dot_q(1,:)';
%% CLIK algorithm execution

%t1=cputime;

if clik_type == 1 % 1st order with Jacobian inverse
    for i=1:(length(int)-1)
                
        %clc;   %%%%%%%% DEBUG %%%%%%%% 
        %disp('First order CLIK with Jacobian inverse...');   %%%%%%%% DEBUG %%%%%%%% 
        %disp('Progress %');   %%%%%%%% DEBUG %%%%%%%% 
        %disp(uint8(int(i)*100/t_f));   %%%%%%%% DEBUG %%%%%%%%     
        
        Xd = X_d(i,:);
        dot_Xd = dot_X_d(i,:);
        
        error(i,:) = pose_error(Xd,Xe(i,:));
        
        j_inv = jacobian_inverse(q(i,:));

        dot_q(i,:) = j_inv*((Kp.*error(i,:))' + dot_Xd');

        q(i+1,:) = q(i,:) + dot_q(i,:)*t_sam;

        Xe(i+1,:) = direct_kinematics(q(i+1,:));
    end
    
elseif clik_type == 2 % 1st order with Jacobian transpose
      for i=1:(length(int)-1)

        %clc;   %%%%%%%% DEBUG %%%%%%%% 
        %disp('First order CLIK with Jacobian transpose...');   %%%%%%%% DEBUG %%%%%%%% 
        %disp('Progress %');   %%%%%%%% DEBUG %%%%%%%% 
        %disp(uint8(int(i)*100/t_f));   %%%%%%%% DEBUG %%%%%%%% 
        
        Xd=X_d(i,:);
        dot_Xd=dot_X_d(i,:);
        
        error(i,:) = pose_error(Xd,Xe(i,:));

        j_t = jacobian(q(i,:))';

        dot_q(i,:) = j_t*(Kp.*error(i,:))';

        q(i+1,:) = q(i,:) + dot_q(i,:)*t_sam;

        Xe(i+1,:) = direct_kinematics(q(i+1,:));


    end

elseif clik_type == 3 % 2nd order with Jacobian inverse
    for i=1:(length(int)-1)

        %clc;   %%%%%%%% DEBUG %%%%%%%% 
        %disp('Second order CLIK inversion...');   %%%%%%%% DEBUG %%%%%%%% 
        %disp('Progress %');   %%%%%%%% DEBUG %%%%%%%% 
        %disp(uint8(int(i)*100/t_f));   %%%%%%%% DEBUG %%%%%%%% 
        
        Xd=X_d(i,:);
        dot_Xd=dot_X_d(i,:);
        dot_dot_Xd=dot_dot_X_d(i,:);

        error(i,:) = pose_error(Xd, Xe(i,:));
        dot_error(i,:) = dot_Xd - dot_Xe(i,:); %% Check!
        
        j_inv = jacobian_inverse(q(i,:));
        
        dot_jacobian = jacobian_time_derivate(q(i,:),dot_q(i,:));
        
        dot_dot_q(i,:) = j_inv * ((Kd.*dot_error(i,:))' + (Kp.*error(i,:))'...
                                   + dot_dot_Xd' - dot_jacobian*dot_q(i,:)'); 

        dot_q(i+1,:) = dot_q(i,:) + dot_dot_q(i,:)*t_sam;

        q(i+1,:) = q(i,:) + dot_q(i,:)*t_sam;

        Xe(i+1,:) = direct_kinematics(q(i+1,:));
        dot_Xe(i+1,:) = jacobian(q(i+1,:))*dot_q(i+1,:)';
        
    end
else
    disp('Mode not supported.');
end

%t2=cputime-t1;   %%%%%%%% DEBUG %%%%%%%% 
%str = ['Finished in ' num2str(t2) ' seconds'];   %%%%%%%% DEBUG %%%%%%%% 
%disp(str);   %%%%%%%% DEBUG %%%%%%%% 
%disp('Joint variables: ');   %%%%%%%% DEBUG %%%%%%%% 
%disp(q(end,:));   %%%%%%%% DEBUG %%%%%%%% 

%disp('Desired pose: ');   %%%%%%%% DEBUG %%%%%%%% 
%disp(Xd);   %%%%%%%% DEBUG %%%%%%%% 

%disp('Reached pose: ');   %%%%%%%% DEBUG %%%%%%%% 
%disp(Xe(end,:));   %%%%%%%% DEBUG %%%%%%%% 

%norm_err = sqrt(error(:,1).^2+error(:,2).^2+error(:,3).^2+error(:,4).^2);   %%%%%%%% DEBUG %%%%%%%% 

%figure(1)   %%%%%%%% DEBUG %%%%%%%% 
%subplot(5,1,1);plot([int(1):t_sam:int(length(int))], Xe(:,1), [int(1):t_sam:int(length(int))], X_d(:,1)); title('X axis');legend('Real', 'Reference');   %%%%%%%% DEBUG %%%%%%%% 
%subplot(5,1,2);plot([int(1):t_sam:int(length(int))], Xe(:,2), [int(1):t_sam:int(length(int))], X_d(:,2)); title('Y axis');legend('Real', 'Reference');   %%%%%%%% DEBUG %%%%%%%% 
%subplot(5,1,3);plot([int(1):t_sam:int(length(int))], Xe(:,3), [int(1):t_sam:int(length(int))], X_d(:,3)); title('Z axis');legend('Real', 'Reference');   %%%%%%%% DEBUG %%%%%%%% 
%subplot(5,1,4);plot([int(1):t_sam:int(length(int))], Xe(:,4), [int(1):t_sam:int(length(int))], X_d(:,4)); title('Orientation');legend('Real', 'Reference');   %%%%%%%% DEBUG %%%%%%%% 
%subplot(5,1,5);plot([int(1):t_sam:int(length(int)-1)], norm_err);title('Error');   %%%%%%%% DEBUG %%%%%%%% 
end
