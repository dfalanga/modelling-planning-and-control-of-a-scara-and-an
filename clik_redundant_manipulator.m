%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% SOSTITUITO CON main_redundant_manipulator
%%%% DA CANCELLARE!!!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load database/path_points.dat;
load database/path_velocities.dat;
load database/orientation.dat;

path_points = path_points(1:5000,2:3);
path_velocities = path_velocities(1:5000,2:3);
orientation = orientation(1:5000,:);

%% Integration parameters (Euler integration)
t_sam = 100*1e-5;
t_i   = 0;                % Initial instant
t_f   = length(path_points)*t_sam-t_sam;
int   = (t_i:t_sam:t_f)'; % Integration interval

%% Gains
Kp = [500 500 500];
%% Desired trajectory in the operative space
X_d(:,1:2) = path_points; % Position for Y and Z
X_d(:,3) = orientation;

dot_X_d(:,1:2) = path_velocities;
dot_X_d(:,3) = zeros(1,length(path_velocities));

k0 = 1;

q(1,:) = [1.3826 -2.0042 -1.0000 0.6216];

dot_q(1,:) = [0 0 0 0];
dot_q0(1,:) = [0 0 0 0];

X_e(1,:) = direct_kinematics(q(1,:));

In = eye(4);

% Kinematic inversion for the redundant manipulator
dof_to_relax = 'x'; % Choose the componente of the operating space to relax
k0 = 1;

 for i=1:(length(int)-1)
        clc;   %%%%%%%% DEBUG %%%%%%%% 
        disp('First order CLIK with Jacobian pseudo-inverse...');   %%%%%%%% DEBUG %%%%%%%% 
        disp('Progress %');   %%%%%%%% DEBUG %%%%%%%% 
        disp(uint8(int(i)*100/t_f));   %%%%%%%% DEBUG %%%%%%%%        
        
        Xd = X_d(i,:);
        dot_Xd = dot_X_d(i,:);
        
        Rot_d = angle2rot(X_d(i,3),0,0);
        Rot_e = angle2rot(X_e(i,4),0,0);
        % Find the relative orientation between desired and actual pose
        Orientation_error_matrix = transpose(Rot_e)*Rot_d;
        % Find the error angle as in (2.22) 
        phi_error = atan2(Orientation_error_matrix(2,1),Orientation_error_matrix(1,1));

        % Position error:
        % The position error is the difference between the two position vectors
        position_error = X_d(i,1:2) - X_e(i, 2:3);  % Xd(Y,Z) - Xe(Y,Z)

        error(i,:) = [position_error phi_error];
        
        j_r = reduced_jacobian(q(i,:),dof_to_relax);
        
        j_pseudo_inv = pinv(j_r);
        
        dw_dq = manipulability_measure(q(i,:));
        
        dot_q0(i,:) = k0 * dw_dq;

        dot_q(i,:) = j_pseudo_inv*((Kp.*error(i,:))' + dot_Xd') + (In - j_pseudo_inv*j_r)*dot_q0(i,:)';
        
        q(i+1,:) = q(i,:) + dot_q(i,:)*t_sam;

        X_e(i+1,:) = direct_kinematics(q(i+1,:));
 end

 
figure(1)
plot3(X_e(:,1), X_e(:,2), X_e(:,3));
 
norm_err = sqrt(error(:,1).^2+error(:,2).^2+error(:,3).^2); 

figure(2)
subplot(2,1,1); plot(int, q), grid, title('Joint space trajectories'), legend('q1', 'q2', 'q3', 'q4');
subplot(2,1,2); plot([int(1):t_sam:int(length(int)-1)], norm_err);title('Error');

