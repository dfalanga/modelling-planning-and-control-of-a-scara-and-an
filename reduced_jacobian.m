% Computes the actual value of the reduced Jacobian

function J = reduced_jacobian(q, dof_to_relax)

    t1 = q(1);
    t2 = q(2);
    d3 = q(3);
    t4 = q(4);

    % Reduced Jacobian (3x4) without the second raw (y axis)

    if strcmp(dof_to_relax,'x')
        J =  [  cos(t1 + t2)/2 + cos(t1)/2         cos(t1 + t2)/2         0           0;
                0                                    0                    1           0;
                1                                    1                    0           1];

    elseif strcmp(dof_to_relax,'y')
        J =  [- sin(t1 + t2)/2 - sin(t1)/2        -sin(t1 + t2)/2         0           0;
                0                                    0                    1           0;
                1                                    1                    0           1];

    elseif strcmp(dof_to_relax,'z')
        J =  [- sin(t1 + t2)/2 - sin(t1)/2        -sin(t1 + t2)/2         0           0;
                cos(t1 + t2)/2 + cos(t1)/2         cos(t1 + t2)/2         0           0;
                1                                    1                    0           1];

    elseif strcmp(dof_to_relax,'phi')
        J =  [- sin(t1 + t2)/2 - sin(t1)/2        -sin(t1 + t2)/2         0           0;
                cos(t1 + t2)/2 + cos(t1)/2         cos(t1 + t2)/2         0           0;
                0                                    0                    1           0];

    end

end