clear all;
close all;
clc;

syms a1 a2 d0;
syms l1 l2 l3 l4;
syms ml1 ml2 ml3 ml4;
syms Il1 Il2 Ix Iy Il3 Il4;
syms kr1 kr2 kr3 kr4;
syms Im1 Im2 Im3 Im4;
syms Fm1 Fm2 Fm3 Fm4;
syms g;

syms t1 t2 d3 t4;
syms dot_t1 dot_t2 dot_d3 dot_t4;
syms dot_dot_q_r_1 dot_dot_q_r_2 dot_dot_q_r_3 dot_dot_q_r_4;
syms dot_q_r_1 dot_q_r_2 dot_q_r_3 dot_q_r_4;
syms tau1 tau2 tau3 tau4;

dot_dot_q_r = [dot_dot_q_r_1; dot_dot_q_r_2; dot_dot_q_r_3; dot_dot_q_r_4];
dot_q_r = [dot_q_r_1; dot_q_r_2; dot_q_r_3; dot_q_r_4];

B(1,1) = Il1 + Il2 + Il3 + Il4 + Im2 + Im3 + Im4 + Im1*kr1^2 + a1^2*ml2 + a1^2*ml3 + a2^2*ml2 + a1^2*ml4 + a2^2*ml3 + a2^2*ml4 + l2^2*ml2 + ml1*(a1 - l1)^2 - 2*a2*l2*ml2 + 2*a1*a2*ml2*cos(t2) + 2*a1*a2*ml3*cos(t2) + 2*a1*a2*ml4*cos(t2) - 2*a1*l2*ml2*cos(t2);
B(1,2) = Il2 + Il3 + Il4 + Im3 + Im4 + Im2*kr2 + a2^2*ml2 + a2^2*ml3 + a2^2*ml4 + l2^2*ml2 - 2*a2*l2*ml2 + a1*a2*ml2*cos(t2) + a1*a2*ml3*cos(t2) + a1*a2*ml4*cos(t2) - a1*l2*ml2*cos(t2);
B(1,3) = Im3*kr3;
B(1,4) = Il4 + Im4*kr4;
B(2,1) = Il2 + Il3 + Il4 + Im3 + Im4 + Im2*kr2 + a2^2*ml2 + a2^2*ml3 + a2^2*ml4 + l2^2*ml2 - 2*a2*l2*ml2 + a1*a2*ml2*cos(t2) + a1*a2*ml3*cos(t2) + a1*a2*ml4*cos(t2) - a1*l2*ml2*cos(t2);
B(2,2) = Il2 + Il3 + Il4 + Im3 + Im4 + Im2*kr2^2 + a2^2*ml3 + a2^2*ml4 + ml2*(a2 - l2)^2;
B(2,3) = Im3*kr3;
B(2,4) = Il4 + Im4*kr4;                                                                                                                                      
B(3,1) = Im3*kr3;
B(3,2) = Im3*kr3;
B(3,3) = Im3*kr3^2 + ml3 + ml4;
B(3,4) = 0;                                                                                                                                                                                                                           
B(4,1) = Il4 + Im4*kr4;                                                                                                                                                                                                                              
B(4,2) = Il4 + Im4*kr4;                                                                                                                                                                                                                                          
B(4,3) = 0;                                                                                                                                                                                                                                          
B(4,4) = Im4*kr4^2 + Il4;
                              
C(1,1)   = -a1*dot_t2*sin(t2)*(a2*ml2 + a2*ml3 + a2*ml4 - l2*ml2);
C(1,2)   = -a1*sin(t2)*(dot_t1 + dot_t2)*(a2*ml2 + a2*ml3 + a2*ml4 - l2*ml2);
C(1,3:4) = 0;
C(2,1)   = -a1*dot_t1*sin(t2)*(a2*ml2 + a2*ml3 + a2*ml4 - l2*ml2);
C(2,2:4) = 0;
C(3:4,:) = zeros(2,4);

F =  diag([Fm1*kr1^2 Fm2*kr2^2 Fm3*kr3^2 Fm4*kr4^2]);
    
gr = [0; 0; g*(ml3+ml4); 0];

tau = B*dot_dot_q_r + C*dot_q_r + F*dot_q_r + gr;

pigr(1) = Il1 + Im1*kr1^2 + a1^2*ml1 + (l1 - 2*a1)*ml1*l1;
pigr(2) = Fm1;
pigr(3) = Il2 + Il3;
pigr(4) = Im2;
pigr(5) = ml2 + ml3 + ml4;
pigr(6) = ml2*l2;
pigr(7) = Fm2;
pigr(8) = Im3;
pigr(9) = Fm3;
pigr(10) = Il4;
pigr(11) = Im4;
pigr(12) = Fm4;
pigr(13) = ml3;
pigr(14) = ml4;

reg = tau/pigr.';
