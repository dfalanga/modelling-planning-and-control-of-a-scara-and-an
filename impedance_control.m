%function [q, dot_q, dot_dot_q, Xe] = impedance_control()
clear all;
close all;
clc;

disp('Initialization...');

disp('Loading operating space trajectories...');
load database/Xd_force.dat;
load database/dot_Xd_force.dat;
load database/dot_dot_Xd_force.dat;
load database/orientation_force.dat;
load database/q0.dat;

% load database/q.dat;
% load database/dot_q.dat;
% load database/dot_dot_q.dat;


Xd = [Xd_force orientation_force];
dot_Xd = [dot_Xd_force orientation_force];
dot_dot_Xd = [dot_dot_Xd_force orientation_force];

Xd(:,1:2) = Xd(:,1:2) + 0.3;

% qd = q(1:5000,:);
% dot_qd = dot_q(1:5000,:);
% dot_dot_qd = dot_dot_q(1:5000,:);

%% Robot inizialization
q_e(1,:) = [1.9180   -2.2653   -1.0000    0.3472];
dot_q_e(1,:) = [0 0 0 0];
dot_dot_q_e(1,:) = [0 0 0 0];
Xe(1,:) = [0.3 0.3 0 0];
dot_Xe(1,:) = zeros(1,4);

%% Robot parameters
a1  = 0.5;
a2  = 0.5;
d0  = 1;
[l1, l2, ml1, ml2, ml3, ml4, Il1, Il2, Il3, Il4, kr1, kr2, kr3, kr4, Im1, Im2, Im3, Im4, Fm1, Fm2, Fm3, Fm4, gr] = load_dynamic_parameters();

%% Control parameters
t_sam = 0.001;
t_i   = 0;
t_f   = length(Xd)*t_sam-t_sam;
int   = (t_i:t_sam:t_f)';

%% Environment
K_plane = 3*10^3;
Z_plane = -0.3;


%% Mechanical impedance
Md = diag([1 1 1 1]);
Kd = diag([70 70 70 70]);
Kp = diag([40 40 40 40]);

%% Robust control
disp('Starting impedance control...');

    for i=1:(length(int)-1)
        
        x_tilde(i,:) = Xd(i,:) - Xe(i,:);
        dot_x_tilde(i,:) = dot_Xd(i,:) - dot_Xe(i,:);

        [B, n, J, dot_J] = dynamic_matrixes(q_e(i,:), dot_q_e(i,:));
        
        if(Xe(i,3)) <= Z_plane
           he(i,:) = zeros(1,4);
           he(i,3) = K_plane*(Xe(i,3) - Z_plane);
        else
           he(i,:) = zeros(1,4);
        end
        
        % Stablizing action
        y(i,:) = (inv(J)*inv(Md)*(Md*dot_dot_Xd(i,:)' + Kd*dot_x_tilde(i,:)' + Kp*x_tilde(i,:)' - Md*dot_J*dot_q_e(i,:)' - he(i,:)'))';

        % Decoupling and linearizing action
        u(i,:) = (B*y(i,:)' + n' + J'*he(i,:)')';
        
        dot_dot_q_e(i,:) = direct_dynamics_force(q_e(i,:)', dot_q_e(i,:)', u(i,:)', he(i,:)');
        dot_q_e(i+1,:) = dot_q_e(i,:) + dot_dot_q_e(i,:)*t_sam;
        q_e(i+1,:) = q_e(i,:) + dot_q_e(i,:)*t_sam;

        Xe(i+1,:) = direct_kinematics(q_e(i+1,:));
        dot_Xe(i+1,:) = (J*dot_q_e(i+1,:)')';
        

    end
    
%% Plot
disp('Control complete, plotting results...');
err_pos = x_tilde(:,1:3);

figure(1)
plot3(Xe(:,1), Xe(:,2), Xe(:,3),'b');
hold on
plot3(Xd(:,1), Xd(:,2), Xd(:,3), 'r') ,legend('Real trajectory','Desired trajectory');

figure(2)
subplot(4,1,1); plot([int(1):t_sam:int(length(int)-1)], u(:,1)), grid, title('Joint 1 torque');
subplot(4,1,2); plot([int(1):t_sam:int(length(int)-1)], u(:,2)), grid, title('Joint 2 torque');
subplot(4,1,3); plot([int(1):t_sam:int(length(int)-1)], u(:,3)), grid, title('Joint 3 torque');
subplot(4,1,4); plot([int(1):t_sam:int(length(int)-1)], u(:,4)), grid, title('Joint 4 torque');

figure(3)
plot([int(1):t_sam:int(length(int)-1)], err_pos), title('Operational space error (position)'), legend('x', 'y', 'z');
%subplot(3,1,2); plot([int(1):t_sam:int(length(int)-1)], q_tilde), title('Joint space error (position)'), legend('q1', 'q2', 'q3', 'q4');
%subplot(3,1,3); plot([int(1):t_sam:int(length(int)-1)], dot_q_tilde), title('Joint space error (velocity)'), legend('q1', 'q2', 'q3', 'q4');

figure(4)
plot([int(1):t_sam:int(length(int)-1)], he);
%end