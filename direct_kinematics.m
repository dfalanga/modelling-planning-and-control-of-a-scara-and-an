% Input = joint variables
% Output = pose of the end effector

function Xe=direct_kinematics(q)

% Finds the vector (x,y,z,phi) as k(q)

t1=q(1);
t2=q(2);
d3=q(3);
t4=q(4);

T = [ -sin(t1 + t2 + t4)    cos(t1 + t2 + t4)       0       cos(t1 + t2)/2 + cos(t1)/2;
      cos(t1 + t2 + t4)     sin(t1 + t2 + t4)       0       sin(t1 + t2)/2 + sin(t1)/2;
      0                      0                       -1       d3 + 1;
      0                      0                        0       1];
              
R_e = T(1:3,1:3);
P_e = T(1:3,4);

phi = atan2(-R_e(1,1),R_e(2,1)); % Angle around z0 axis

Xe=[P_e; phi]; 

end