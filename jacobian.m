% Input: q
% Output: Jacobian(q)

function J = jacobian(q)

t1 = q(1);
t2 = q(2);
d3 = q(3);
t4 = q(4);

% Reduced Jacobian (4x4)
J =  [- sin(t1 + t2)/2 - sin(t1)/2        -sin(t1 + t2)/2        0           0;
       cos(t1 + t2)/2 + cos(t1)/2         cos(t1 + t2)/2         0           0;
       0                                    0                    1           0;
       1                                    1                    0           1];

end