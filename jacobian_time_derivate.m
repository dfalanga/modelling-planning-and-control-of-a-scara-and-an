% Input: q
% Output: Jacobian derivate

function jacobian_derivate = jacobian_time_derivate(q,dot_q)
t1=q(1);
t2=q(2);
dot_t1=dot_q(1);
dot_t2=dot_q(2);
    
jacobian_derivate = [ - cos(t1 + t2)*(dot_t1+dot_t2) - cos(t1)*dot_t1/2    -cos(t1 + t2)*(dot_t1 + dot_t2)    0    0;
                      - sin(t1 + t2)*(dot_t1+dot_t2) - sin(t1)*dot_t1/2    -sin(t1 + t2)*(dot_t1+dot_t2)      0    0;
                      0                                                       0                               0    0;
                      0                                                       0                               0    0];
end