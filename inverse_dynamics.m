function tau = inverse_dynamics(q, dot_q, dot_dot_q, he)

a1  = 0.5;
a2  = 0.5;
d0  = 1;

[l1, l2, ml1, ml2, ml3, ml4, Il1, Il2, Il3, Il4, kr1, kr2, kr3, kr4, Im1, Im2, Im3, Im4, Fm1, Fm2, Fm3, Fm4, gr] = load_dynamic_parameters();


t1 = q(1);
t2 = q(2);
d3 = q(3);
t4 = q(4);

dot_t1 = q(1);
dot_t2 = q(2);
dot_d3 = q(3);
dot_t4 = q(4);

B(1,1) = Il1 + Il2 + Il3 + Il4 + Im2 + Im3 + Im4 + ml1*(a1/2 - l1)^2 + Im1*kr1^2 + (a1^2*ml2)/4 + (a1^2*ml3)/4 + (a2^2*ml2)/4 + (a1^2*ml4)/4 + (a2^2*ml3)/4 + (a2^2*ml4)/4 + l2^2*ml2 - a2*l2*ml2 + (a1*a2*ml2*cos(t2))/2 + (a1*a2*ml3*cos(t2))/2 + (a1*a2*ml4*cos(t2))/2 - a1*l2*ml2*cos(t2);
B(1,2) = Il2 + Il3 + Il4 + Im3 + Im4 + Im2*kr2 + (a2^2*ml2)/4 + (a2^2*ml3)/4 + (a2^2*ml4)/4 + l2^2*ml2 - a2*l2*ml2 + (a1*a2*ml2*cos(t2))/4 + (a1*a2*ml3*cos(t2))/4 + (a1*a2*ml4*cos(t2))/4 - (a1*l2*ml2*cos(t2))/2;
B(1,3) = Im3*kr3;
B(1,4) = Il4 + Im4*kr4;
B(2,1) = Il2 + Il3 + Il4 + Im3 + Im4 + Im2*kr2 + (a2^2*ml2)/4 + (a2^2*ml3)/4 + (a2^2*ml4)/4 + l2^2*ml2 - a2*l2*ml2 + (a1*a2*ml2*cos(t2))/4 + (a1*a2*ml3*cos(t2))/4 + (a1*a2*ml4*cos(t2))/4 - (a1*l2*ml2*cos(t2))/2;
B(2,2) = Il2 + Il3 + Il4 + Im3 + Im4 + ml2*(a2/2 - l2)^2 + Im2*kr2^2 + (a2^2*ml3)/4 + (a2^2*ml4)/4;
B(2,3) = Im3*kr3;
B(2,4) = Il4 + Im4*kr4;
B(3,1) = Im3*kr3;
B(3,2) = Im3*kr3;
B(3,3) = Im3*kr3^2 + ml3 + ml4;
B(3,4) = 0;
B(4,1) = Il4 + Im4*kr4;
B(4,2) = Il4 + Im4*kr4;
B(4,3) = 0;
B(4,4) = Im4*kr4^2 + Il4;

C(1,1)   = -(a1*dot_t2*sin(t2)*(a2*ml2 + a2*ml3 + a2*ml4 - 2*l2*ml2))/4;
C(1,2)   = -(a1*sin(t2)*(dot_t1 + dot_t2)*(a2*ml2 + a2*ml3 + a2*ml4 - 2*l2*ml2))/4;
C(1,3:4) = 0;
C(2,1)   = -(a1*dot_t1*sin(t2)*(a2*ml2 + a2*ml3 + a2*ml4 - 2*l2*ml2))/4
C(2,2:4) = 0;
C(3:4,:) = zeros(2,4);

F    =  diag([Fm1*kr1^2 Fm2*kr2^2 Fm3*kr3^2 Fm4*kr4^2]);

g    =  [0; 0; gr*ml3 + gr*ml4; 0];

J    = jacobian(q);

% Output
tau = B*dot_dot_q + C*dot_q + F*dot_q + g + transpose(J)*he;


end