% p_i : punto iniziale
% p_f : punto finale
% a_max : accelerazione massima
% t : tempo in cui eseguire la traiettoria

function [p, dot_p, dot_dot_p] = rect_trajectory(segment,a_max,t)

    for j=1:length(segment)
    tj=t_tot+t(j);    
    s_0(j,) = zeros(size(0:Ts:tj));
    
    s_f = sqrt((p_f(1) - p_i(1))^2 + (p_f(2) - p_i(2))^2);
    [s(j,:),sp(j,:),spp(j,:)]=vel_trap(s_i,s_f,t,a_max);
    
    for i=1:length(s)
    p(i,1) = p_i(1) + s(i)*(p_f(1) - p_i(1))/(s_f);
    p(i,2) = p_i(2) + s(i)*(p_f(2) - p_i(2))/(s_f);
    p(i,3) = p_i(3) + s(i)*(p_f(3) - p_i(3))/(s_f);

    dot_p(i,1) = sp(i)*(p_f(1) - p_i(1))/(s_f);
    dot_p(i,2) = sp(i)*(p_f(2) - p_i(2))/(s_f);
    dot_p(i,3) = sp(i)*(p_f(3) - p_i(3))/(s_f);

    dot_dot_p(i,1) = 0;
    dot_dot_p(i,2) = 0;
    dot_dot_p(i,3) = 0;
    end
    
end