
load database/path_points.dat;
load database/path_velocities.dat;
load database/orientation.dat;

n=5000;

% Integration parameters (Euler integration)
t_sam = 100*1e-5;
t_i   = 0;                % Initial instant
t_f   = n*t_sam-t_sam;
int   = (t_i:t_sam:t_f)'; % Integration interval

path_points(1:n,3) = sin(2*pi*int);

% Gains
Kp = [500 500 500];
k0 = 50;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% Kinematic inversion relaxing X %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Relaxing x...');
path_points_x = path_points(1:5000,2:3);
path_velocities_x = path_velocities(1:5000,2:3);
orientation_x = orientation(1:5000,:);

X_d(:,1:2) = path_points_x;
X_d(:,3) = orientation_x;

dot_X_d(:,1:2) = path_velocities_x;
dot_X_d(:,3) = zeros(1,length(path_velocities_x));


q(1,:) = [1.3826 -2.0042 -1.0000 0.6216];

dot_q(1,:) = [0 0 0 0];
dot_q0(1,:) = [0 0 0 0];

X_e(1,:) = direct_kinematics(q(1,:));

In = eye(4);

dof_to_relax = 'x';

 for i=1:(length(int)-1)

        Xd = X_d(i,:);
        dot_Xd = dot_X_d(i,:);
        
        Rot_d = angle2rot(X_d(i,3),0,0);
        Rot_e = angle2rot(X_e(i,4),0,0);
        Orientation_error_matrix = transpose(Rot_e)*Rot_d;
        phi_error = atan2(Orientation_error_matrix(2,1),Orientation_error_matrix(1,1));

        position_error = X_d(i,1:2) - X_e(i, 2:3);
        error(i,:) = [position_error phi_error];
        
        j_r = reduced_jacobian(q(i,:),dof_to_relax);
        
        j_pseudo_inv = pinv(j_r);
        
        dw_dq = manipulability_measure(q(i,:));
        
        dot_q0(i,:) = k0 * dw_dq;

        dot_q(i,:) = j_pseudo_inv*((Kp.*error(i,:))' + dot_Xd') + (In - j_pseudo_inv*j_r)*dot_q0(i,:)';
        
        q(i+1,:) = q(i,:) + dot_q(i,:)*t_sam;

        X_e(i+1,:) = direct_kinematics(q(i+1,:));
 end
 
norm_err = sqrt(error(:,1).^2+error(:,2).^2+error(:,3).^2);
q_x = q;
error_x = norm_err;
X_e_x = X_e;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% Kinematic inversion relaxing Y %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Relaxing y...');
path_points_y = path_points(1:5000,1:2:3);
path_velocities_y = path_velocities(1:5000,1:2:3);
orientation_y = orientation(1:5000,:);

X_d(:,1:2) = path_points_y;
X_d(:,3) = orientation_y;

dot_X_d(:,1:2) = path_velocities_y;
dot_X_d(:,3) = zeros(1,length(path_velocities_y));

q(1,:) = [1.3826 -2.0042 -1.0000 0.6216];

dot_q(1,:) = [0 0 0 0];
dot_q0(1,:) = [0 0 0 0];

X_e(1,:) = direct_kinematics(q(1,:));

In = eye(4);

dof_to_relax = 'y';

 for i=1:(length(int)-1)       
        
        Xd = X_d(i,:);
        dot_Xd = dot_X_d(i,:);
        
        Rot_d = angle2rot(X_d(i,3),0,0);
        Rot_e = angle2rot(X_e(i,4),0,0);
        Orientation_error_matrix = transpose(Rot_e)*Rot_d;
        phi_error = atan2(Orientation_error_matrix(2,1),Orientation_error_matrix(1,1));

        position_error = X_d(i,1:2) - X_e(i, 1:2:3);

        error(i,:) = [position_error phi_error];
        
        j_r = reduced_jacobian(q(i,:),dof_to_relax);
        
        j_pseudo_inv = pinv(j_r);
        
        dw_dq = manipulability_measure(q(i,:));
        
        dot_q0(i,:) = k0 * dw_dq;

        dot_q(i,:) = j_pseudo_inv*((Kp.*error(i,:))' + dot_Xd') + (In - j_pseudo_inv*j_r)*dot_q0(i,:)';
        
        q(i+1,:) = q(i,:) + dot_q(i,:)*t_sam;

        X_e(i+1,:) = direct_kinematics(q(i+1,:));
 end
norm_err = sqrt(error(:,1).^2+error(:,2).^2+error(:,3).^2);
q_y = q;
error_y = norm_err;
X_e_y = X_e;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% Kinematic inversion relaxing Z %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Relaxing z...');
path_points_z = path_points(1:5000,1:2);
path_velocities_z = path_velocities(1:5000,1:2);
orientation_z = orientation(1:5000,:);

X_d(:,1:2) = path_points_z;
X_d(:,3) = orientation_z;

dot_X_d(:,1:2) = path_velocities_z;
dot_X_d(:,3) = zeros(1,length(path_velocities_z));

q(1,:) = [1.3826 -2.0042 -1.0000 0.6216];

dot_q(1,:) = [0 0 0 0];
dot_q0(1,:) = [0 0 0 0];

X_e(1,:) = direct_kinematics(q(1,:));

In = eye(4);

dof_to_relax = 'z';

 for i=1:(length(int)-1)
        %clc;   %%%%%%%% DEBUG %%%%%%%% 
        %disp('First order CLIK with Jacobian pseudo-inverse...');   %%%%%%%% DEBUG %%%%%%%% 
        %disp('Progress %');   %%%%%%%% DEBUG %%%%%%%% 
        %disp(uint8(int(i)*100/t_f));   %%%%%%%% DEBUG %%%%%%%%        
        
        Xd = X_d(i,:);
        dot_Xd = dot_X_d(i,:);
        
        Rot_d = angle2rot(X_d(i,3),0,0);
        Rot_e = angle2rot(X_e(i,4),0,0);
        % Find the relative orientation between desired and actual pose
        Orientation_error_matrix = transpose(Rot_e)*Rot_d;
        % Find the error angle as in (2.22) 
        phi_error = atan2(Orientation_error_matrix(2,1),Orientation_error_matrix(1,1));

        % Position error:
        % The position error is the difference between the two position vectors
        position_error = X_d(i,1:2) - X_e(i, 1:2);

        error(i,:) = [position_error phi_error];
        
        j_r = reduced_jacobian(q(i,:),dof_to_relax);
        
        j_pseudo_inv = pinv(j_r);
        
        dw_dq = manipulability_measure(q(i,:));
        
        dot_q0(i,:) = k0 * dw_dq;

        dot_q(i,:) = j_pseudo_inv*((Kp.*error(i,:))' + dot_Xd') + (In - j_pseudo_inv*j_r)*dot_q0(i,:)';
        
        q(i+1,:) = q(i,:) + dot_q(i,:)*t_sam;

        X_e(i+1,:) = direct_kinematics(q(i+1,:));
 end
 
norm_err = sqrt(error(:,1).^2+error(:,2).^2+error(:,3).^2);
q_z = q;
error_z = norm_err;
X_e_z = X_e;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%% Kinematic inversion relaxing Phi %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Relaxing phi...');
path_points_phi = path_points(1:5000,1:3);
path_velocities_phi = path_velocities(1:5000,1:3);

X_d(:,1:3) = path_points_phi;
dot_X_d(:,1:3) = path_velocities_phi;

q(1,:) = [1.3826 -2.0042 -1.0000 0.6216];

dot_q(1,:) = [0 0 0 0];
dot_q0(1,:) = [0 0 0 0];

X_e(1,:) = direct_kinematics(q(1,:));

In = eye(4);

dof_to_relax = 'phi';

 for i=1:(length(int)-1)    
        
        Xd = X_d(i,:);
        dot_Xd = dot_X_d(i,:);
        
        position_error = X_d(i,1:3) - X_e(i, 1:3);
        
        error(i,:) = [position_error];
        
        j_r = reduced_jacobian(q(i,:),dof_to_relax);
        
        j_pseudo_inv = pinv(j_r);
        
        dw_dq = manipulability_measure(q(i,:));
        
        dot_q0(i,:) = k0 * dw_dq;

        dot_q(i,:) = j_pseudo_inv*((Kp.*error(i,:))' + dot_Xd') + (In - j_pseudo_inv*j_r)*dot_q0(i,:)';
        
        q(i+1,:) = q(i,:) + dot_q(i,:)*t_sam;

        X_e(i+1,:) = direct_kinematics(q(i+1,:));
 end
 
norm_err = sqrt(error(:,1).^2+error(:,2).^2+error(:,3).^2);

q_phi = q;
error_phi = norm_err;
X_e_phi = X_e;
 


% Plot
figure(1)
subplot(4,1,1); plot(int, q_x), grid, title('Joint space trajectories relaxing x component'), legend('q1', 'q2', 'q3', 'q4');
subplot(4,1,2); plot(int, q_y), grid, title('Joint space trajectories relaxing y component'), legend('q1', 'q2', 'q3', 'q4');
subplot(4,1,3); plot(int, q_z), grid, title('Joint space trajectories relaxing z component'), legend('q1', 'q2', 'q3', 'q4');
subplot(4,1,4); plot(int, q_phi), grid, title('Joint space trajectories relaxing phi component'), legend('q1', 'q2', 'q3', 'q4');

figure(2)
subplot(4,1,1); plot([int(1):t_sam:int(length(int)-1)], error_x);title('Error relaxing x component');
subplot(4,1,2); plot([int(1):t_sam:int(length(int)-1)], error_y);title('Error relaxing y component');
subplot(4,1,3); plot([int(1):t_sam:int(length(int)-1)], error_z);title('Error relaxing z component');
subplot(4,1,4); plot([int(1):t_sam:int(length(int)-1)], error_phi);title('Error relaxing phi component');
figure(3)
plot3(X_e_x(:,1), X_e_x(:,2), X_e_x(:,3)), title('Operating space trajectory relaxing x component');
figure(4)
plot3(X_e_y(:,1), X_e_y(:,2), X_e_y(:,3)), title('Operating space trajectory relaxing y component');
figure(5)
plot3(X_e_z(:,1), X_e_z(:,2), X_e_z(:,3)), title('Operating space trajectory relaxing z component');
figure(6)
plot3(X_e_phi(:,1), X_e_phi(:,2), X_e_phi(:,3)), title('Operating space trajectory relaxing phi component');
