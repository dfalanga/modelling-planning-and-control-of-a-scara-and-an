function y = regressor(q, dot_q, dot_q_r, dot_dot_q_r)
a1  = 0.5;
a2  = 0.5;
d0  = 1;
[l1, l2, ml1, ml2, ml3, ml4, Il1, Il2, Il3, Il4, kr1, kr2, kr3, kr4, Im1, Im2, Im3, Im4, Fm1, Fm2, Fm3, Fm4, gr] = load_dynamic_parameters();

y  = zeros(4,14);

%% Cesare
% y(1,1)=dot_dot_q_r(1);
% y(1,2)=kr1^2*dot_q_r(1);
% y(1,3)=dot_dot_q_r(1)+dot_dot_q_r(2);
% y(1,4)=dot_dot_q_r(1)+dot_dot_q_r(2)*kr2;
% y(1,5)=(a1^2+a2^2+2*a1*a2*cos(q(2)))*dot_dot_q_r(1)+(a2^2+a1*a2*cos(q(2)))*dot_dot_q_r(2)-a1*a2*sin(q(2))*(dot_q(2)*dot_q_r(1)+(dot_q(1)+dot_q(2))*dot_q_r(2));
% y(1,6)=(l2-2*a2-2*a1*cos(q(2)))*dot_dot_q_r(1)+(l2-2*a2-a1*cos(q(2)))*dot_dot_q_r(2)+a1*sin(q(2))*(dot_q(2)*dot_q_r(1)+(dot_q(1)+dot_q(2))*dot_q_r(2));
% y(1,8)=dot_dot_q_r(1)+dot_dot_q_r(2)+kr3*dot_dot_q_r(3);
% y(1,10)=dot_dot_q_r(1)+dot_dot_q_r(2)+dot_dot_q_r(4);
% y(1,11)=dot_dot_q_r(1)+dot_dot_q_r(2)+dot_dot_q_r(4)*kr4;
% y(2,3)=y(1,3);
% y(2,4)=kr2*y(1,4);
% y(2,5)=(a2^2+a1*a2*cos(q(2)))*dot_dot_q_r(1)+a2^2*dot_dot_q_r(2)+(a1*a2*sin(q(2))*dot_q(1))*dot_q_r(1);
% y(2,6)=(l2-2*a2-a1*cos(q(2)))*dot_dot_q_r(1)+(l2-2*a2)*dot_dot_q_r(2)-(a1*sin(q(2))*dot_q(1))*dot_q_r(1);
% y(2,7)=kr2^2*dot_q_r(2);
% y(2,8)=y(1,8);
% y(2,10)=y(1,10);
% y(2,11)=y(1,11);
% y(3,8)=kr3*y(1,8);
% y(3,9)=kr3^2*dot_q_r(3);
% y(3,13)=dot_dot_q_r(3)+gr;
% y(3,14)=y(3,13);
% y(4,10)=y(1,10);
% y(4,11)=kr4*y(1,11);
% y(4,12)=kr4^2*dot_q_r(4);

%% Calcolato

y(1,1) = dot_dot_q_r(1);
y(1,2) = kr1^2*dot_q_r(1);
%y(1,2) = kr2^2*dot_q_r(1);
y(1,3) = dot_dot_q_r(1) + dot_dot_q_r(2);
y(1,4) = dot_dot_q_r(1) + kr2*dot_q_r(2);
%y(1,4) = dot_dot_q_r(1) + kr2^2*dot_q_r(2);
y(1,5) = (a1^2 + a2^2 + 2*a1*a2*cos(q(2)))*dot_dot_q_r(1) + (a2^2 + a1*a2*cos(q(2)))*dot_dot_q_r(2) - a1*a2*sin(q(2))*(dot_q(2)*dot_q_r(1) + (dot_q(1) + dot_q(2))*dot_q_r(2));
%y(1,5) = (a1^2 + a2^2 + 2*a1*a2*cos(q(2)))*dot_dot_q_r(1) + (a2^2 + 2*a1*a2*cos(q(2)))*dot_dot_q_r(2) - a1*a2*sin(q(2))*(dot_q(2)*dot_q_r(1) + (dot_q(1) + dot_q(2))*dot_q_r(2));
y(1,6) = (l2 - 2*a2 - 2*a1*cos(q(2)))*dot_dot_q_r(1) + (l2 - 2*a2 - a1*cos(q(2)))*dot_dot_q_r(2) + a1*sin(q(2))*(dot_q(2)*dot_q_r(1) + (dot_q(1) + dot_q(2))*dot_q_r(2));
y(1,8) = dot_dot_q_r(1) + dot_dot_q_r(2) + kr3*dot_dot_q_r(3);
y(1,10) = dot_dot_q_r(1) + dot_dot_q_r(2) + dot_dot_q_r(4);
y(1,11) = dot_dot_q_r(1) + dot_dot_q_r(2) + kr4*dot_dot_q_r(4);

y(2,3) = y(1,3);
y(2,4) = kr2*y(1,4);
y(2,5) = (a2^2 + a1*a2*cos(q(2)))*dot_dot_q_r(1) + a2^2*dot_dot_q_r(2) + (a1*a2*sin(q(2))*dot_q(1))*dot_q_r(1);
%y(2,5) = (a2^2 + 2*a1*a2*cos(q(2)))*dot_dot_q_r(1) + a2*dot_dot_q_r(2) + (a1*a2*sin(q(2))*dot_q(1))*dot_q_r(1);
y(2,6) = (l2 - 2*a2 - a1*cos(q(2)))*dot_dot_q_r(1) + (l2 - 2*a2)*dot_dot_q_r(2) - (a1*sin(q(2))*dot_q(1))*dot_q_r(1);
y(2,7) = kr2^2*dot_q_r(2);
y(2,8) = y(1,8);
y(2,10) = y(1,10);
y(2,11) = y(1,11);

y(3,8) = kr3*y(1,8);
%y(3,8) = kr2*y(1,8);
y(3,9) = kr3^2*dot_q_r(3);
%y(3,9) = kr3*dot_q_r(3);
y(3,13) = dot_dot_q_r(3) + gr;
y(3,14) = dot_dot_q_r(3) + gr;

y(4,10) = y(1,10);
y(4,11) = kr4*y(1,11);
y(4,12) = kr4^2*dot_q_r(4);


end

