% For circular paths (via points not allowed)
% * points{i}.type = 'c'
% * points{i}.t_i = starting time
% * points{i}.t_f = ending time
% * points{i}.args.p_i = starting point
% * points{i}.args.c = center
% * points{i}.args.angle = angle
% * points{i}.args.axis = rotation matrix
% * points{i}.par = parametric representation of the path (par.s, par.ds,
%                   par.dds)
% * points{i}.path = path in the space (path.p, path.dp, path.ddp)


% For rectilinear paths
% * points{i}.type = 'r'
% * points{i}.t_i = starting time
% * points{i}.t_f = ending time
% * points{i}.args.p_i = starting point
% * points{i}.args.p_f = ending point
% * points{i}.args.via = 0/1;
% * points{i}.args.dt = for via points
% * points{i}.par = parametric representation of the path
% * points{i}.path = path in the space

function [path, dpath, ddpath, orientation] = trajectory()
    %% Global parameters and useful variables
    Ts = 0.001;  % Sampling time
    a_max = 0.5; % Max acceleration
    Dt_tot=0;

    %% Create the path struct
    % Circular
    points{1}.type = 'c';
    points{1}.t_i = 0;
    points{1}.t_f = 5;
    points{1}.args.p_i = [0.5, 0.2, 0];
    points{1}.args.c = [0.5, 0, 0];
    points{1}.args.angle = 2*pi;
    points{1}.args.axis = [0 0 1];

    % Circular
    points{2}.type = 'c';
    points{2}.t_i = 5;
    points{2}.t_f = 8;
    points{2}.args.p_i = [0.5, 0.2, 0];
    points{2}.args.c = [0.5, 0.1, 0];
    points{2}.args.angle = pi;
    points{2}.args.axis = [0 0 1];
    
    % Circular
    points{3}.type = 'c';
    points{3}.t_i = 8;
    points{3}.t_f = 11;
    points{3}.args.p_i = [0.5, 0, 0];
    points{3}.args.c = [0.5, -0.1, 0];
    points{3}.args.angle = -pi;
    points{3}.args.axis = [0 0 -1];
    
    % Linear (via point)  
    points{4}.type = 'r';
    points{4}.t_i = 11;
    points{4}.t_f = 13;
    points{4}.args.p_i = [0.5, -0.2, 0];
    points{4}.args.p_f = [0.5, -0.175, 0.2];
    points{4}.args.via = 0;
    points{4}.args.dt = 0;

    points{5}.type = 'r';
    points{5}.t_i = 13;
    points{5}.t_f = 15;
    points{5}.args.p_i = [0.5, -0.175, 0.2];
    points{5}.args.p_f = [0.5, -0.15, 0];
    points{5}.args.via = 1;
    points{5}.args.dt = 0.1;
    
    % Circular
    points{6}.type = 'c';
    points{6}.t_i = 15;
    points{6}.t_f = 18;
    points{6}.args.p_i = [0.5, -0.15, 0];
    points{6}.args.c = [0.5, -0.125, 0];
    points{6}.args.angle = 2*pi;
    points{6}.args.axis = [0 0 1];
    
    % Linear (via point) 
    points{7}.type = 'r';
    points{7}.t_i = 18;
    points{7}.t_f = 21;
    points{7}.args.p_i = [0.5, -0.15, 0];
    points{7}.args.p_f = [0.5, 0, 0.2];
    points{7}.args.via = 0;
    points{7}.args.dt = 0;

    points{8}.type = 'r';
    points{8}.t_i = 21;
    points{8}.t_f = 24;
    points{8}.args.p_i = [0.5, 0, 0.2];
    points{8}.args.p_f = [0.5, 0.1, 0];
    points{8}.args.via = 1;
    points{8}.args.dt = 0.1;

    % Circular
    points{9}.type = 'c';
    points{9}.t_i = 24;
    points{9}.t_f = 27;
    points{9}.args.p_i = [0.5, 0.1, 0];
    points{9}.args.c = [0.5, 0.125, 0];
    points{9}.args.angle = 2*pi;
    points{9}.args.axis = [0 0 1];


%%% TEST DELLE FUNZIONI

%      points{1}.type = 'r';
%      points{1}.t_i = 0;
%      points{1}.t_f = 10;
%      points{1}.args.p_i = [0, 0, 0];
%      points{1}.args.p_f = [3, 5, 0];
%      points{1}.args.via = 0;
%      points{1}.args.dt = 0;
     
%      points{2}.type = 'r';
%      points{2}.t_i = 10;
%      points{2}.t_f = 20;
%      points{2}.args.p_i = [3, 5, 0];
%      points{2}.args.p_f = [5, 2, 0];
%      points{2}.args.via = 0;
%      points{2}.args.dt = 0;
     
%      points{3}.type = 'r';
%      points{3}.t_i = 20;
%      points{3}.t_f = 30;
%      points{3}.args.p_i = [5, 2, 0];
%      points{3}.args.p_f = [7, 8, 0];
%      points{3}.args.via = 0;
%      points{3}.args.dt = 0;
%      
%      points{4}.type = 'r';
%      points{4}.t_i = 30;
%      points{4}.t_f = 40;
%      points{4}.args.p_i = [7, 8, 0];
%      points{4}.args.p_f = [10, 6, 0];
%      points{4}.args.via = 0;
%      points{4}.args.dt = 0;
     
    %% Create the parametric representation of the path
    n = length(points);
    t_tot = 0;

    for j=1:n
        t_tot = t_tot + (points{j}.t_f - points{j}.t_i);
    end

    for j=1:n
        
        if strcmp(points{j}.type,'r')

            s_f=norm(points{j}.args.p_f - points{j}.args.p_i);
            s_i=0;

            [points{j}.par.s,points{j}.par.ds, points{j}.par.dds,Dt] = vel_trap_mod(s_f, s_i, Ts, ...
                                                                            points{j}.t_i, points{j}.t_f, t_tot, points{j}.args.via, a_max, Dt_tot);

            if points{j}.args.via == 1
                Dt_tot = Dt_tot + Dt;
            end

        elseif strcmp(points{j}.type,'c')
            rho = norm(points{j}.args.p_i - points{j}.args.c);
            s_i = 0;
            s_f = norm(rho*points{j}.args.angle);

            [points{j}.par.s,points{j}.par.ds, points{j}.par.dds, Dt] = vel_trap_mod(s_f, s_i, Ts, points{j}.t_i, points{j}.t_f, t_tot, 0, a_max, Dt_tot);

        else
            disp('Error: path type not recognized (only rectilinear or circular path allowed)');
            return
        end

    end


    %% Create the path in the space

    for j=1:n
        
        if strcmp(points{j}.type,'r')
            [points{j}.path.p, points{j}.path.dp, points{j}.path.ddp] = rect_path_mod(points{j}.par, points{j}.args.p_i, points{j}.args.p_f);

        elseif strcmp(points{j}.type,'c')
            rho = norm(points{j}.args.p_i - points{j}.args.c);
            [points{j}.path.p, points{j}.path.dp, points{j}.path.ddp] = circ_path_mod(points{j}.par, ...
                                                                            points{j}.args.p_i, points{j}.args.c, rho, a_max, points{j}.args.axis);
        
            %%Risolvo il problema della traslazione della traiettoria sottraendo il punto iniziale per tutti i path diversi dal primo                                                            
            if j>1
                points{j}.path.p(:,1)=points{j}.path.p(:,1)- points{j}.args.p_i(1);
                points{j}.path.p(:,2)=points{j}.path.p(:,2)- points{j}.args.p_i(2);
                points{j}.path.p(:,3)=points{j}.path.p(:,3)- points{j}.args.p_i(3);
            end
            
        else
            disp('Error: path type not recognized (only rectilinear or circular path allowed)');
            return
        end

    end

    path=zeros(length(points{1}.path.p), 3);
    dpath=zeros(length(points{1}.path.p), 3);
    ddpath=zeros(length(points{1}.path.p), 3);

    for j=1:n
        
        path = path + points{j}.path.p;
        dpath = dpath + points{j}.path.dp;
        ddpath = ddpath + points{j}.path.ddp;

    end
    
    orientation = zeros(length(path),1);
    %plot3(path(:,1),path(:,2),path(:,3));   %%%%%%%% DEBUG %%%%%%%% 
    %figure   %%%%%%%% DEBUG %%%%%%%% 
    %plot(path(:,1),path(:,2));   %%%%%%%% DEBUG %%%%%%%% 
    
%     plot(points{1}.par.ds);
%     hold on;
%     plot(points{2}.par.ds);
%     hold on;
%     plot(points{3}.par.ds);
%     
%     figure()
%     plot(path(:,1), path(:,2))
end