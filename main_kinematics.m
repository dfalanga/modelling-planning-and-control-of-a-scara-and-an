% Main program

clc;
close all;
clear all;

% Load path points, velocities and accelerations. Path has been planned
% using trajectory.m file, which requires lot of time to complete path
% generation. For this reason the full path has been stored in .dat files
% to speed up the execution of kinematics inversion.

disp('Welcome to the main program for the kinematic simulation of a SCARA robot');

disp('First, you have to choose how to plan the trajectory. You can compute it or just load it from .dat files.');

trajectory_choice = input('[1] Compute trajectory (it needs time) [2] Load from file... ');

% Load kinematics values from .dat file previously stored
if trajectory_choice == 2
    disp('Loading operating space trajectories...');
    load database/path_points.dat;
    load database/path_velocities.dat;
    load database/path_accelerations.dat;
    load database/orientation.dat;

    disp('Loading joint space trajectories...');
    load database/q.dat;
    load database/q0.dat;
    load database/dot_q.dat;
    load database/dot_dot_q.dat;
    
    disp('Loading direct kinematics...');
    load database/Xe.dat;

    disp('Loading complete!');
    
    t_sam = 100*1e-5;
    t_i   = 0;  
    t_f   = length(path_points)*t_sam-t_sam;  % Final instant
    int   = t_i:t_sam:t_f; % Integration interval

else
    
% Computes kinematics values    

  disp('Creating path in the space...');
  [path_points, path_velocities, path_accelerations, orientation] = trajectory();

    % Integration and kinematic inversion parameters
    t_sam = 100*1e-5;
    t_i   = 0;  
    t_f   = length(path_points)*t_sam-t_sam;
    int   = t_i:t_sam:t_f;

    clik_type = 3;

    % Kinematic initialization
    q0 = [0 -pi/2 0 0];
    k(:,1)=path_points(1,1)*ones(1,100);
    k(:,2)=path_points(1,2)*ones(1,100);
    k(:,3)=path_points(1,3)*ones(1,100);
    orient_init = zeros(1,100);

    disp('Kinematic initialization...');
    [q_0, dot_q_0, dot_dot_q_0, Xe_0] = clik_algorithm(1, k, zeros(100,4), zeros(100,4), orient_init, q0, t_sam);

    % Kinematic inversion
    disp('Kinematic inversion...');
    [q, dot_q, dot_dot_q, Xe] = clik_algorithm(clik_type, path_points, path_velocities, path_accelerations, orientation, q_0(end,:), t_sam);

    disp('Kinematic inversion complete!');
end

% Evaluate the error norm
Xd = [path_points orientation];

for i=1:(length(int)-1)
    error(i,:) = pose_error(Xd(i,:),Xe(i,:));
end

norm_err = sqrt(error(:,1).^2+error(:,2).^2+error(:,3).^2+error(:,4).^2); 

% Plot joint space trajectories and error norm
figure(1)
subplot(2,1,1); plot(int, q), grid, title('Joint space trajectories'), legend('q1', 'q2', 'q3', 'q4');
subplot(2,1,2); plot([int(1):t_sam:int(length(int)-1)], norm_err);title('Error');

% Plot operating space trajectories (both 2D and 3D)
figure(2)
plot3(Xe(:,1), Xe(:,2), Xe(:,3)), title('Operating space trajectories (3D)');

figure(3)
plot(Xe(:,1), Xe(:,2)), title('Operating space trajectories (2D, XY plane)');

% Plot manipulability ellipsoid for
figure(4)
for i=1:5
    
joints=q(i*1000,:);
[veloc_ellips, force_ellips]= manipulability_ellipsoid(joints);

plot(veloc_ellips(:,1),veloc_ellips(:,2));
hold on
plot(force_ellips(:,1),force_ellips(:,2),'r');
hold on
    
end


figure(5)
plot(int, dot_q), grid, title('Joint space velocities'), legend('q1', 'q2', 'q3', 'q4');



