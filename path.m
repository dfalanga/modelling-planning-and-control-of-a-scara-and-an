%Ripulisce la memoria e lo schermo
clc
clear
close all

%Periodo di campionamento, posizione iniziale
a = [0.5; 0.5; 0.5];
Tc = 0.001;
Ts = 2*Tc;

%Vettore dei tempi
T = (0:Tc:3.999)';

%Traiettoria di riferimento (posizione e velocit?))
xd = [0.25*(1-cos(pi*T)) 0.25*(2+sin(pi*T)) sin((pi/24)*T)];
dxd = pi*[0.25*sin(pi*T) 0.25*cos(pi*T) (1/24)*cos((pi/24)*T)];

%Posizione iniziale nello spazio dei giunti
q0 =pi*[1;-0.5;-0.5];

%Completa la traiettoria dinamica con un tratto costante di durata 1 sec.
T1 = (4:Tc:5)';
xd1 = ones(size(T1,1),1)*[0 0.5 sin(pi/6)];
dxd1 = zeros(size(T1,1),3);

T = [T;T1];
xd = [xd;xd1];
dxd = [dxd;dxd1];