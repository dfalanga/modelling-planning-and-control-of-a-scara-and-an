% Input: K*e + dot_xd (first 4 elements) and q (last 4 elements)
% Output: dot_q

function J_inv = jacobian_inverse(q)

t1 = q(1);
t2 = q(2);
d3 = q(3);
t4 = q(4);

% Reduced Jacobian (4x4)
% J_r = [- sin(t1 + t2)/2 - sin(t1)/2      -sin(t1 + t2)/2        0           0;
%       cos(t1 + t2)/2 + cos(t1)/2         cos(t1 + t2)/2         0           0;
%       0                                    0                       1           0;
%       1                                    1                       0           1];

% Inverse of Jacobian matrix
J_inv = [-(2*cos(t1 + t2))/(cos(t1 + t2)*sin(t1) - sin(t1 + t2)*cos(t1))...
         -(2*sin(t1 + t2))/(cos(t1 + t2)*sin(t1) - sin(t1 + t2)*cos(t1))...
         0 0;
         
         (2*cos(t1 + t2) + cos(t1))/(cos(t1 + t2)*sin(t1) - sin(t1 + t2)*cos(t1))...
         (2*sin(t1 + t2) + sin(t1))/(cos(t1 + t2)*sin(t1) - sin(t1 + t2)*cos(t1))...
         0 0;
         
         0 0 1 0;
         
         -cos(t1)/(cos(t1 + t2)*sin(t1) - sin(t1 + t2)*cos(t1))...
         -sin(t1)/(cos(t1 + t2)*sin(t1) - sin(t1 + t2)*cos(t1))...
         0 1];

end