%function [q, dot_q, dot_dot_q, Xe, par_extim] = adaptive_control()
clear all;
close all;
clc;
disp('Initialization...');

disp('Loading operating space trajectories...');
load database/path_points.dat;
load database/path_velocities.dat;
load database/path_accelerations.dat;
%load database/orientation.dat;

disp('Loading joint space trajectories...');
load database/q.dat;
load database/q0.dat;
load database/dot_q.dat;
load database/dot_dot_q.dat;
n = 5000;
%% Trajectories
% Desired trajectories
q_d = q(1:n, :);
dot_q_d = dot_q(1:n, :);
dot_dot_q_d = dot_dot_q(1:n, :);

% Robot inizialization
q_e(1,:) = q0;
dot_q_e(1,:) = [0 0 0 0];
dot_dot_q_e(1,:) = [0 0 0 0];

%% Robot parameters
a1  = 0.5;
a2  = 0.5;
d0  = 1;
[l1, l2, ml1, ml2, ml3, ml4, Il1, Il2, Il3, Il4, kr1, kr2, kr3, kr4, Im1, Im2, Im3, Im4, Fm1, Fm2, Fm3, Fm4, gr] = load_dynamic_parameters();

%% Control parameters
t_sam = 0.001;
t_i   = 0;
t_f   = length(q_d)*t_sam-t_sam;
int   = (t_i:t_sam:t_f)';

% Gains
Kd  = [2000 2000 5000 2000];
lambda = [5 5 5 5];
Kpig = eye(14,14);
Kpig(14,14) = 0.001;
%Kpig_const = eye(13);                %%%Prova con adattamento solo sull'ultimo parametro
%Kpig_extim = 0.001;                  %%%Prova con adattamento solo sull'ultimo parametro

% Generalized forces on the end effector
he  = zeros(4,1);

% Extimation initialization
par_extim(1,:) = parameters_extimation();
dot_par_extim(1,:) = zeros(1,14);
%par_extim(1,:) = 0;                         %%%Prova con adattamento solo sull'ultimo parametro
%dot_par_extim(1,:) = 0;                     %%%Prova con adattamento solo sull'ultimo parametro
%const_param = parameters_extimation();      %%%Prova con adattamento solo sull'ultimo parametro
%const_param = const_param(1:13)';           %%%Prova con adattamento solo sull'ultimo parametro

%% Adaptive control
disp('Starting control...');

    for i=1:(length(int)-1) 
        % Position and velocity error
        dot_q_tilde(i,:) = dot_q_d(i,:) - dot_q_e(i,:);
        q_tilde(i,:) = q_d(i,:) - q_e(i,:);
        
        dot_q_r(i,:) = dot_q_d(i,:) + (lambda*q_tilde(i,:)')';
        dot_dot_q_r(i,:) = dot_dot_q_d(i,:) + (lambda*dot_q_tilde(i,:)')';

        sigma(i,:) = dot_q_r(i,:) - dot_q_e(i,:);
        
        y = regressor(q_e(i,:), dot_q_e(i,:), dot_q_r(i,:), dot_dot_q_r(i,:));
                
        dot_par_extim(i,:) = (inv(Kpig)*y'*sigma(i,:)')';
        par_extim(i+1,:) = par_extim(i,:) + dot_par_extim(i,:)*t_sam;
        %dot_par_extim(i,:) = (inv(Kpig_extim)*y(:,14)'*sigma(i,:)')';  %%%Prova con adattamento solo sull'ultimo parametro
        %par_extim(i+1,:) = par_extim(i,:) + dot_par_extim(i,:)*t_sam   %%%Prova con adattamento solo sull'ultimo parametro
        
        u(i,:) = (y*par_extim(i,:)')' + (Kd*sigma(i,:)')';
        %u(i,:) = (y(:,14)*par_extim(i,:)')' + (Kd*sigma(i,:)')' + (y(:,1:13)*const_param)';  %%%Prova con adattamento solo sull'ultimo parametro
        
        dot_dot_q_e(i,:) = direct_dynamics(q_e(i,:)', dot_q_e(i,:)', u(i,:)', he);
        
        dot_q_e(i+1,:) = dot_q_e(i,:) + dot_dot_q_e(i,:)*t_sam;
        q_e(i+1,:) = q_e(i,:) + dot_q_e(i,:)*t_sam;
        
        Xe(i,:) = direct_kinematics(q_e(i,:));
        
    end
    
%% Plot
disp('Control complete, plotting results...');
%err_pos = Xe(:,1:3) - path_points(1:end-1,:);
err_pos = Xe(:,1:3) - path_points(1:n-1,:);

figure(1)
plot3(Xe(:,1), Xe(:,2), Xe(:,3),'b');
hold on
plot3(path_points(1:n,1), path_points(1:n,2), path_points(1:n,3), 'r') ,legend('Real trajectory','Desired trajectory');

figure(2)
subplot(2,1,1); plot(int, q_e), grid, title('Joint space trajectories (real)'), legend('q1', 'q2', 'q3', 'q4');
subplot(2,1,2); plot(int, q_d), grid, title('Joint space trajectories (desired)'), legend('q1', 'q2', 'q3', 'q4');

figure(3)
subplot(4,1,1); plot([int(1):t_sam:int(length(int)-1)], u(:,1)), grid, title('Joint 1 torque');
subplot(4,1,2); plot([int(1):t_sam:int(length(int)-1)], u(:,2)), grid, title('Joint 2 torque');
subplot(4,1,3); plot([int(1):t_sam:int(length(int)-1)], u(:,3)), grid, title('Joint 3 torque');
subplot(4,1,4); plot([int(1):t_sam:int(length(int)-1)], u(:,4)), grid, title('Joint 4 torque');

figure(4)
subplot(3,1,1); plot([int(1):t_sam:int(length(int)-1)], err_pos), title('Operational space error (position)'), legend('x', 'y', 'z');
subplot(3,1,2); plot([int(1):t_sam:int(length(int)-1)], q_tilde), title('Joint space error (position)'), legend('q1', 'q2', 'q3', 'q4');
subplot(3,1,3); plot([int(1):t_sam:int(length(int)-1)], dot_q_tilde), title('Joint space error (velocity)'), legend('q1', 'q2', 'q3', 'q4');

%end