% Qui stiamo testando i via

clc
clear all;
close all;
p(1,:) = [0 0 0];
p(2,:) = [0.2 0.1 0];
p(3,:) = [0.2 0.03 0];
p(4,:) = [0.3 0.2 0];
p(5,:) = [0.4 0.02 0];
p(6,:) = [0.5 0.4 0];
p(7,:) = [0.6 0.1 0];
p(8,:) = [0.7 0.4 0];

via(1)=0;
via(2)=1;
via(3)=1;
via(4)=1;
via(5)=1;
via(6)=1;
via(7)=1;
via(8)=0;

t(1)=0;
t(2)=5;
t(3)=10;
t(4)=15;
t(5)=20;
t(6)=25;
t(7)=30;
t(8)=35;

Ts=0.001;
a_max=0.4;
p_tot=p(1,:);
Dt_tot=0;

for i=1:(length(t)-1)
   
    sf=norm(p(i+1,:)-p(i,:));
    si=0;
   
    [s(i,:),sp(i,:),spp(i,:),Dt]=vel_trap_mod(sf,si,Ts,t(i),t(i+1),t(end),via(i),a_max,Dt_tot);
    if via(i)==1
        Dt_tot=Dt_tot+Dt;
    end
      
end

for i=1:length(s)
    p_e1(:,i) = s(1,i)*(p(2,:)-p(1,:))/norm(p(2,:)-p(1,:));
    p_e2(:,i) = s(2,i)*(p(3,:)-p(2,:))/norm(p(3,:)-p(2,:));
    p_e3(:,i) = s(3,i)*(p(4,:)-p(3,:))/norm(p(4,:)-p(3,:));
    p_e4(:,i) = s(4,i)*(p(5,:)-p(4,:))/norm(p(5,:)-p(4,:));
    p_e5(:,i) = s(5,i)*(p(6,:)-p(5,:))/norm(p(6,:)-p(5,:));
    p_e6(:,i) = s(6,i)*(p(7,:)-p(6,:))/norm(p(7,:)-p(6,:));
    p_e7(:,i) = s(7,i)*(p(8,:)-p(7,:))/norm(p(8,:)-p(7,:));
end

p_tot=p_e1(:,:)+p_e2(:,:)+p_e3(:,:)+p_e4(:,:)+p_e5(:,:)+p_e6(:,:)+p_e7(:,:);
plot(p_tot(1,:),p_tot(2,:)),grid;
%figure
%plot(p_tot(1,:)),grid;
%figure
%plot(p_tot(2,:)),grid;