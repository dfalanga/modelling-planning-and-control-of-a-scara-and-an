clear all;
clc;

DH = [0.5 0.5 0 0; 1 0 0 0; 0 0 0 0; 0 0 0 0]';
Tipe_axis=['R'; 'R'; 'P'; 'R'];
n_giunti = 4;

% Calcolo le matrici di trasformazione omogenea tra le terne. La struct A
% contiene per ogni elementi i la matrice di trasformazione tra la terna
% i-1 e la terna i
for i=1:n_giunti
%Giunto rotoidale
if strcmp(Tipe_axis(i),'R')
    %setto i parametri di DH per il giunto i (theta ? variabile)
    a=DH(i,1);
    d=DH(i,2);
    alpha=DH(i,3);
    theta = sym(strcat('t',num2str(i)));
    v_diff(i)=theta;
    
    %Calcolo la matrice di trasformazione tra il giunto i e i-1
    %Utilizzo sind e cosd per i parametri angolari che non sono variabili
    %in modo da eliminare il problema di round_off
   A{i}=...
   [cos(theta) -sin(theta)*cosd(alpha) sin(theta)*sind(alpha) a*cos(theta);
    sin(theta) cos(theta)*cosd(alpha)  -cos(theta)*sind(alpha) a*sin(theta);
    0 sind(alpha) cosd(alpha) d;
    0 0 0 1;
    ];

%Giunto prismatico
elseif strcmp(Tipe_axis(i),'P')
    %setto i parametri di DH per il giunto i (d ? variabile)
    a=DH(i,1);
    d = sym(strcat('d',num2str(i)));
    alpha=DH(i,3);
    theta=DH(i,4);
    v_diff(i)=d;
    
    %Calcolo la matrice di trasformazione tra il giunto i e i-1
    %Utilizzo sind e cosd per i parametri angolari che non sono variabili
    %in modo da eliminare il problema di round_off
    A{i}=...
    [cosd(theta) -sind(theta)*cosd(alpha) sind(theta)*sind(alpha) a*cosd(theta);
     sind(theta) cosd(theta)*cosd(alpha)  -cosd(theta)*sind(alpha) a*sind(theta);
     0 sind(alpha) cosd(alpha) d;
     0 0 0 1;
     ];
end
end

A_e=[0 1 0 0; 1 0 0 0; 0 0 -1 0; 0 0 0 1];

% Calcolo la matrice di trasformazione tra la terna zero e quella
% dell'end-effector
T{1} = A{1};
for i=2:n_giunti
    T{i} = T{i-1} * A{i};
   
end
T_0_e=simplify(T{n_giunti}*A_e);

%q=[15;22;0;35];
%%K=dirkin(q,T_0_e);
%Calcolo dello Jacobiano geometrico 

for i=0:n_giunti-1
    if i==0
       p_im1=[0;0;0];
       z_im1=[0;0;1];
    else
       z_im1 = T{i}(1:3,3);
       p_im1 = T{i}(1:3,4);
    end
     p_e = T_0_e(1:3,4);
     
     if strcmp(Tipe_axis(i+1),'R') % Giunto rotoidale
       JP{i+1}=cross(z_im1,(p_e-p_im1));
       JO{i+1}=z_im1;
     elseif strcmp(Tipe_axis(i+1),'P') % Giunto prismatico
       JP{i+1}=z_im1;
       JO{i+1}=zeros(3,1);
     end
     
     for j=1:3
       J(j,i+1) = JP{i+1}(j); % Salvo la parte di posizione nello Jacobiano
       J(j+3,i+1) = JO{i+1}(j); % Salvo la parte di orientamento nello Jacobiano
        
     end
     
         
end

 J=simplify(J);
 JR=[J(1:3,:); J(6,:)];


%J = [- sin(t1 + t2) - sin(t1)/2         -sin(t1 + t2)       0           0;
%     cos(t1 + t2) + cos(t1)/2           cos(t1 + t2)        0           0;
%     0                                  0                   1           0;
%     0                                  0                   0           0;
%	 0                                  0                   0           0;
%     1                                  1                   0           1];


%J_r =[- sin(t1 + t2) - sin(t1)/2         -sin(t1 + t2)      0           0;
%    cos(t1 + t2) + cos(t1)/2           cos(t1 + t2)        0           0;
%    0                                  0                   1           0;
%    1                                  1                   0           1];