%% Simboli presenti nelle matrici
syms t1 t2 d3 t4;
syms a1 a2 d0;
syms dot_t1 dot_t2 dot_d3 dot_t4;
syms l1 l2 l3 l4;
syms ml1 ml2 ml3 ml4;
syms Il1 Il2 Ix Iy Il3 Il4;
syms kr1 kr2 kr3 kr4;
syms Im1 Im2 Im3 Im4;
syms Fm1 Fm2 Fm3 Fm4;
syms g;

%% Matrici di trasformazione, usate per calcolare e riportare in terna 0 gli jacobiani

A01 = [ cos(t1)     -sin(t1)        0       a1*cos(t1);
        sin(t1)     cos(t1)         0       a1*sin(t1);
        0           0               1       d0;
        0           0               0       1];

    
    
A12 = [ cos(t2)     -sin(t2)        0       a2*cos(t2);
        sin(t2)     cos(t2)         0       a2*sin(t2);
        0           0               1       0;
        0           0               0       1];


A23 = [ 1  0  0   0;
        0  1  0   0;
        0  0  1  d3;
        0  0  0   1];



A34 = [ cos(t4)     -sin(t4)        0       0;
        sin(t4)     cos(t4)         0       0;
        0           0               1       0;
        0           0               0       1];
    

A4e = [ 0  1  0   0;
        1  0  0   0;
        0  0  -1  0;
        0  0  0   1];
    
    
A02 = simple(A01*A12);
A03 = simple(A02*A23);
A04 = simple(A03*A34);
A0e = simple(A04*A4e);

p0  = A0e(1:3,4);
p1  = A0e(1:3,4)-A01(1:3,4);
p2  = A0e(1:3,4)-A02(1:3,4);
p3  = A0e(1:3,4)-A03(1:3,4);

z0  = [0 0 1]';
z1  = A01(1:3,3);
z2  = A02(1:3,3);
z3  = A03(1:3,3);

%% Inertia matrix (B)

% Braccio 1
% Link
pl1   =  simple(A01*[-l1;0;0;1]);                 % Vettore posizione
Jp1   =  simple([cross(z0,pl1(1:3)) zeros(3,3)]); % Jacobiano di posizione
Jo1   =  [z0 zeros(3,3)];                         % Jacobiano di orientamento
R01   =  A01(1:3,1:3);                            % Matrice di rotazione per riportare i due jacobiani
I     =  [0 0 0;0 Il1 0;0 0 Il1];                 % Matrice di inerzia
Bl1   =  simple(ml1*Jp1.'*Jp1 + Jo1.'*R01*I*R01.'*Jo1);
% Motore
Jo1m  =  [kr1*z0 zeros(3,3)];
Im    =  [10 0 0;0 10 0;0 0 Im1];
Bm1   =  simple(Jo1m.'*Im*Jo1m);
% Totale
B1    =  simple(Bl1+Bm1);

% Braccio 2
% %Link
pl2   =  simple(A02*[-l2;0;0;1]);
Jp2   =  simple([cross(z0,pl2(1:3)) cross(z1,pl2(1:3)-A01(1:3,4)) zeros(3,2)]);
Jo2   =  simple([z0 z1 zeros(3,2)]);
R02   =  A02(1:3,1:3);
I     =  [0 0 0;0 Il2 0;0 0 Il2];
Bl2   =  simple(ml2*Jp2.'*Jp2 + Jo2.'*R02*I*R02.'*Jo2);
% Motore
Jo2m  =  simple([z0 kr2*z1 zeros(3,2)]);
Im    =  [10 0 0;0 10 0;0 0 Im2];
Bm2   =  simple(Jo2m.'*Im*Jo2m);
% Totale
B2    =  Bl2+Bm2;

% Braccio 3
% Link
pl3   =  simple(A03*[0;0;l3;1]);
Jp3   =  simple([cross(z0,pl3(1:3)) cross(z1,pl3(1:3)-A01(1:3,4)) z2 zeros(3,1)]);
Jo3   =  simple([z0 z1 zeros(3,2)]);
R03   =  A03(1:3,1:3);
I     =  [Ix 0 0;0 Iy 0;0 0 Il3];
Bl3   =  simple(ml3*Jp3.'*Jp3 + Jo3.'*R03*I*R03.'*Jo3);
% Motore
Jo3m  =  simple([z0 z1 kr3*z2 zeros(3,1)]);
Im    =  [10 0 0;0 10 0;0 0 Im3];
Bm3   =  simple(Jo3m.'*Im*Jo3m);
% Totale
B3    =  Bl3+Bm3;

% Braccio 4
% Link
pl4   =  simple(A03*[0;0;l4;1]);
Jp4   =  simple([cross(z0,pl4(1:3)) cross(z1,pl4(1:3)-A01(1:3,4)) z2 cross(z3,pl4(1:3)-A03(1:3,4))]);
Jo4   =  simple([z0 z1 zeros(3,1) z3]);
R04   =  A0e(1:3,1:3);
I     =  [Ix 0 0;0 Iy 0;0 0 Il4];
Bl4   =  simple(ml4*Jp4.'*Jp4 + Jo4.'*R04*I*R04.'*Jo4);
% Motore
Jo4m  =  simple([z0 z1 zeros(3,1) kr4*z3]);
Im    =  [10 0 0;0 10 0;0 0 Im4];
Bm4   =  simple(Jo4m.'*Im*Jo4m);
% Totale
B4    =  Bl4+Bm4;

% B Matrix
B     =  simple(B1 + B2 + B3 + B4);


% Risultato:

% B(1,1) = Il1 + Il2 + Il3 + Il4 + Im2 + Im3 + Im4 + Im1*kr1^2 + a1^2*ml2 + a1^2*ml3 + a2^2*ml2 + a1^2*ml4 + a2^2*ml3 + a2^2*ml4 + l2^2*ml2 + ml1*(a1 - l1)^2 - 2*a2*l2*ml2 + 2*a1*a2*ml2*cos(t2) + 2*a1*a2*ml3*cos(t2) + 2*a1*a2*ml4*cos(t2) - 2*a1*l2*ml2*cos(t2);
% B(1,2) = Il2 + Il3 + Il4 + Im3 + Im4 + Im2*kr2 + a2^2*ml2 + a2^2*ml3 + a2^2*ml4 + l2^2*ml2 - 2*a2*l2*ml2 + a1*a2*ml2*cos(t2) + a1*a2*ml3*cos(t2) + a1*a2*ml4*cos(t2) - a1*l2*ml2*cos(t2);
% B(1,3) = Im3*kr3;
% B(1,4) = Il4 + Im4*kr4;
% 
% B(2,1) = Il2 + Il3 + Il4 + Im3 + Im4 + Im2*kr2 + a2^2*ml2 + a2^2*ml3 + a2^2*ml4 + l2^2*ml2 - 2*a2*l2*ml2 + a1*a2*ml2*cos(t2) + a1*a2*ml3*cos(t2) + a1*a2*ml4*cos(t2) - a1*l2*ml2*cos(t2);
% B(2,2) = Il2 + Il3 + Il4 + Im3 + Im4 + Im2*kr2^2 + a2^2*ml3 + a2^2*ml4 + ml2*(a2 - l2)^2;
% B(2,3) = Im3*kr3;
% B(2,4) = Il4 + Im4*kr4;
%                                                                                                                                            
% B(3,1) = Im3*kr3;
% B(3,2) = Im3*kr3;
% B(3,3) = Im3*kr3^2 + ml3 + ml4;
% B(3,4) = 0;
%                                                                                                                                                                                                                                      
% B(4,1) = Il4 + Im4*kr4;                                                                                                                                                                                                                              
% B(4,2) = Il4 + Im4*kr4;                                                                                                                                                                                                                                          
% B(4,3) = 0;                                                                                                                                                                                                                                          
% B(4,4) = Im4*kr4^2 + Il4;
                                                                                                                                                                                                                                            
 

%% Coriolis and centrifugal (C)

% Per quanto riguarda le prime due righe, la matrice B dipende soltanto da
% theta2 (B(1,1),B (1,2) e B(2,1)), dunque soltanto gli analoghi elementi
% della matrice C sono diversi da zero

C(1,1) = simple(0.5*diff(B(1,1),t2)*dot_t2);
C(1,2) = simple(0.5*diff(B(1,1),t2)*dot_t1 + diff(B(1,2),t2)*dot_t2);
C(1,3:4) = 0;

C(2,1) = simple(0.5*diff(B(1,1),t2)*dot_t1);
C(2,2:4) = 0;

% Le righe 3-4 della matrice B non dipendono dalle variabili di giunto,
% dunque le loro derivate rispetto alle stesse sono nulle. Di conseguenza,
% anche le ultime due righe della matrice C sono nulle

C(3:4,:) = zeros(2,4);


% Risultato:

% C(1,1)   = -a1*dot_t2*sin(t2)*(a2*ml2 + a2*ml3 + a2*ml4 - l2*ml2);
% C(1,2)   = -a1*sin(t2)*(dot_t1 + dot_t2)*(a2*ml2 + a2*ml3 + a2*ml4 - l2*ml2);
% C(1,3:4) = 0;
% C(2,1)   = -a1*dot_t1*sin(t2)*(a2*ml2 + a2*ml3 + a2*ml4 - l2*ml2);
% C(2,2:4) = 0;
% C(3:4,:) = zeros(2,4);

%% Friction (F)

Fv   =  diag([Fm1*kr1^2 Fm2*kr2^2 Fm3*kr3^2 Fm4*kr4^2]);

%% Gravity (g)

g0   =  [0; 0; -g];
gd   =  ml1*g0.'*Jp1(1:3,3) + ml2*g0.'*Jp2(1:3,3) + ml3*g0.'*Jp3(1:3,3) + ml4*g0.'*Jp4(1:3,3);

gr   =  [0; 0; -gd; 0];
