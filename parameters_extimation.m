function pigr = parameters_extimation()

[l1, l2, ml1, ml2, ml3, ml4, Il1, Il2, Il3, Il4, kr1, kr2, kr3, kr4, Im1, Im2, Im3, Im4, Fm1, Fm2, Fm3, Fm4, gr] = load_dynamic_parameters();

a1  = 0.5;
a2  = 0.5;
d0  = 1;

pigr(1) = Il1 + Im1*kr1^2 + a1^2*ml1 + (l1 - 2*a1)*ml1*l1;
pigr(2) = Fm1;
pigr(3) = Il2 + Il3;
pigr(4) = Im2;
pigr(5) = ml2 + ml3 + ml4;
pigr(6) = ml2*l2;
pigr(7) = Fm2;
pigr(8) = Im3;
pigr(9) = Fm3;
pigr(10) = Il4;
pigr(11) = Im4;
pigr(12) = Fm4;
pigr(13) = ml3;
pigr(14) = ml4;

end