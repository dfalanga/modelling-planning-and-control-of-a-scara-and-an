% Test program for manipulability ellipsoids

q=[pi/2 pi/3 0 0];
[veloc_ellips, force_ellips]= manipulability_ellipsoid(q)

figure
plot(veloc_ellips(:,1),veloc_ellips(:,2));
hold on
plot(force_ellips(:,1),force_ellips(:,2),'r');
%axis([-1.5 1.5 -1.5 1.5]);
hold on


q=[-pi/2 -pi/3 0 0];
[veloc_ellips, force_ellips]= manipulability_ellipsoid(q)

plot(veloc_ellips(:,1),veloc_ellips(:,2));
hold on
plot(force_ellips(:,1),force_ellips(:,2),'r');
%axis([-1.5 1.5 -1.5 1.5]);
hold on