%function [q, dot_q, dot_dot_q, Xe] = robust_control()
disp('Initialization...');

disp('Loading operating space trajectories...');
load database/path_points.dat;
load database/path_velocities.dat;
load database/path_accelerations.dat;
%load database/orientation.dat;

disp('Loading joint space trajectories...');
load database/q.dat;
load database/q0.dat;
load database/dot_q.dat;
load database/dot_dot_q.dat;

%% Trajectories
% Desired trajectories
q_d = q;
dot_q_d = dot_q;
dot_dot_q_d = dot_dot_q;

% Robot inizialization
q_e(1,:) = q0;
dot_q_e(1,:) = [0 0 0 0];
dot_dot_q_e(1,:) = [0 0 0 0];

%% Robot parameters
a1  = 0.5;
a2  = 0.5;
d0  = 1;
[l1, l2, ml1, ml2, ml3, ml4, Il1, Il2, Il3, Il4, kr1, kr2, kr3, kr4, Im1, Im2, Im3, Im4, Fm1, Fm2, Fm3, Fm4, gr] = load_dynamic_parameters();
%ml4=4;
%% Control parameters
t_sam = 0.001;
t_i   = 0;
t_f   = length(q)*t_sam-t_sam;
int   = (t_i:t_sam:t_f)';

% Gains
Q2  = [1000 1000 1000 1000];
Q3  = [500 500 500 500];
Kp  = [100 100 5000 100];
Kd  = [50 50 50 50];

% Generalized forces on the end effector
he  = zeros(4,1);

% Tolerance
epsilon = 2;

% Unit vector control gain
rho  = 10;

% Imperfect compensation (B diagonal and constant, C ignored)
B_app      =  zeros(4,4);
B_app(1,1) =  Il1 + Il2 + Il3 + Il4 + Im2 + Im3 + Im4 + Im1*kr1^2 + a1^2*ml2 + a1^2*ml3 + a2^2*ml2 + ...
                a1^2*ml4 + a2^2*ml3 + a2^2*ml4 + l2^2*ml2 + ml1*(a1 - l1)^2 - 2*a2*l2*ml2;
B_app(2,2) =  Il2 + Il3 + Il4 + Im3 + Im4 + Im2*kr2^2 + a2^2*ml3 + a2^2*ml4 + ml2*(a2 - l2)^2;
B_app(3,3) =  Im3*kr3^2 + ml3 + ml4;
B_app(4,4) =  Im4*kr4^2 + Il4;
F_app      =  diag([Fm1*kr1^2 Fm2*kr2^2 Fm3*kr3^2 Fm4*kr4^2]);
g_app      =  [0 0 gr*(ml3+ml4) 0];

%% Robust control
disp('Starting robust control...');

    for i=1:(length(int)-1)
        
        % Position and velocity error
        dot_q_tilde(i,:) = dot_q_d(i,:) - dot_q_e(i,:);
        q_tilde(i,:) = q_d(i,:) - q_e(i,:);
        
        % Unit vector action
        z(i,:) = Q2*q_tilde(i,:)' + Q3*dot_q_tilde(i,:)';
        
        if norm(z(i,:)) < epsilon
            w(i,:) = rho*z(i,:)/epsilon;
        else
            w(i,:) = rho*z(i,:)/norm(z(i,:));
        end
        
        % Stablizing action
        y(i,:) = w(i,:) + dot_dot_q_d(i,:) + Kd.*dot_q_tilde(i,:) + Kp.*q_tilde(i,:);
        
        % Decoupling and linearizing action
        u(i,:) = (B_app*y(i,:)')' + (F_app*dot_q_e(i,:)')' + g_app;
        
        
        dot_dot_q_e(i,:) = direct_dynamics(q_e(i,:)', dot_q_e(i,:)', u(i,:)', he);
        
        dot_q_e(i+1,:) = dot_q_e(i,:) + dot_dot_q_e(i,:)*t_sam;
        q_e(i+1,:) = q_e(i,:) + dot_q_e(i,:)*t_sam;
        
        Xe(i,:) = direct_kinematics(q_e(i,:));
        
    end
    
%% Plot
disp('Control complete, plotting results...');
err_pos = Xe(:,1:3) - path_points(1:end-1,:);

figure(1)
plot3(Xe(:,1), Xe(:,2), Xe(:,3),'b'), legend('Real trajectory');
hold on
plot3(path_points(:,1), path_points(:,2), path_points(:,3), 'r') ,legend('Desired trajectory');

figure(2)
subplot(2,1,1); plot(int, q_e), grid, title('Joint space trajectories (real)'), legend('q1', 'q2', 'q3', 'q4');
subplot(2,1,2); plot(int, q_d), grid, title('Joint space trajectories (desired)'), legend('q1', 'q2', 'q3', 'q4');

figure(3)
subplot(4,1,1); plot([int(1):t_sam:int(length(int)-1)], u(:,1)), grid, title('Joint 1 torque');
subplot(4,1,2); plot([int(1):t_sam:int(length(int)-1)], u(:,2)), grid, title('Joint 2 torque');
subplot(4,1,3); plot([int(1):t_sam:int(length(int)-1)], u(:,3)), grid, title('Joint 3 torque');
subplot(4,1,4); plot([int(1):t_sam:int(length(int)-1)], u(:,4)), grid, title('Joint 4 torque');

figure(4)
subplot(3,1,1); plot([int(1):t_sam:int(length(int)-1)], err_pos), title('Operational space error (position)'), legend('x', 'y', 'z');
subplot(3,1,2); plot([int(1):t_sam:int(length(int)-1)], q_tilde), title('Joint space error (position)'), legend('q1', 'q2', 'q3', 'q4');
subplot(3,1,3); plot([int(1):t_sam:int(length(int)-1)], dot_q_tilde), title('Joint space error (velocity)'), legend('q1', 'q2', 'q3', 'q4');

%end