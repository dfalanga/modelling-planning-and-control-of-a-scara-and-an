% p_i : punto iniziale
% p_f : punto finale
% a_max : accelerazione massima
% t : tempo in cui eseguire la traiettoria

function [p, dot_p, dot_dot_p] = rect_path_mod(param_rapp, p_i, p_f)
    s = param_rapp.s;
    ds = param_rapp.ds;
    dds = param_rapp.dds;
    
    s_f = norm(p_f - p_i);
    
    for i=1:length(s)
    p(i,1) =  s(i)*(p_f(1) - p_i(1))/(s_f);
    p(i,2) =  s(i)*(p_f(2) - p_i(2))/(s_f);
    p(i,3) =  s(i)*(p_f(3) - p_i(3))/(s_f);

    dot_p(i,1) = ds(i)*(p_f(1) - p_i(1))/(s_f);
    dot_p(i,2) = ds(i)*(p_f(2) - p_i(2))/(s_f);
    dot_p(i,3) = ds(i)*(p_f(3) - p_i(3))/(s_f);

    dot_dot_p(i,1) = dds(i)*(p_f(1) - p_i(1))/(s_f);
    dot_dot_p(i,2) = dds(i)*(p_f(2) - p_i(2))/(s_f);
    dot_dot_p(i,3) = dds(i)*(p_f(3) - p_i(3))/(s_f);
    end
    
end