function [l1, l2, ml1, ml2, ml3, ml4, Il1, Il2, Il3, Il4, kr1, kr2, kr3, kr4, Im1, Im2, Im3, Im4, Fm1, Fm2, Fm3, Fm4, gr] = load_dynamic_parameters()

l1  = 0.25;
l2  = 0.25;
ml1  = 25;
ml2  = 25;
ml3  = 10;
ml4  = 0;
Il1  = 5;
Il2  = 5;
Il3  = 0;
Il4  = 1;
kr1  = 1;
kr2  = 1;
kr3  = 50;
kr4  = 20;
Im1  = 0.02;
Im2  = 0.02;
Im3  = 0.005;
Im4  = 0.001;
Fm1  = 0.0001;
Fm2  = 0.0001;
Fm3  = 0.01;
Fm4  = 0.005;
gr  = 9.81;

end