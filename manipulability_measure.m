% Calculate in symbolic form the manipulability measure function

% Input: q
% Output: Jacobian(q)

function dw_dq = manipulability_measure(q)
%% Symbolic computation
%t1 = sym('t1');
%t2 = sym('t2');
%d3 = sym('d3');
%t4 = sym('t4');

%J =  [- sin(t1 + t2)/2 - sin(t1)/2        -sin(t1 + t2)/2        0           0;
%       cos(t1 + t2)/2 + cos(t1)/2         cos(t1 + t2)/2         0           0;
%       0                                    0                    1           0;
%       1                                    1                    0           1];

%w = sqrt(det(J*J.'));

%dw_dq(1) = diff(w,t1);
%dw_dq(2) = diff(w,t2);
%dw_dq(3) = diff(w,d3);
%dw_dq(4) = diff(w,t4);

%dw_dq = simplify(dw_dq);

%% Numerical computation
t1 = q(1);
t2 = q(2);
d3 = q(3);
t4 = q(4);

dw_dq = [0 ; sin(2*t2)/(8*(sin(t2)^2)^(1/2)); 0; 0];

end