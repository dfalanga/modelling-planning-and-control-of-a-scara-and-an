% Computes the manipulability ellipsoid (velocity and force)

function [veloc_ellips, force_ellips]= manipulability_ellipsoid(q)

%Calcolo ellissoidi
Xe=direct_kinematics(q);
J=jacobian(q);

[V,E]= eig(J(1:3,:)*J(1:3,:)');
sigma(1)=real(sqrt(E(1,1)));
sigma(2)=real(sqrt(E(2,2)));
sigma(3)=real(sqrt(E(3,3)));
n=40;

[X,Y,Z]=ellipsoid(0,0,0,sigma(1),sigma(2),sigma(3),n-1);

%Rototraslazione ellissoide 3D
for i=1:n
  P=V*[X(:,i)';Y(:,i)';Z(:,i)'];
  X_r(:,i)=P(1,:)+ones(size(P(1,:)))*Xe(1);
  Y_r(:,i)=P(2,:)+ones(size(P(2,:)))*Xe(2);
  Z_r(:,i)=P(3,:)+ones(size(P(3,:)))*Xe(3);
  
end
%surf(X_r,Y_r,Z_r);
%axis([-1.5 1.5 -1.5 1.5 -1.5 1.5]);

%Solo piano XY
t=0:0.01:2*pi;

%Velocit?
X_xy_v=sigma(1)*cos(t);
Y_xy_v=sigma(2)*sin(t);
P_xy=V*[X_xy_v; Y_xy_v; zeros(size(X_xy_v))];
X_xy_v_r=P_xy(1,:)+ones(size(P_xy(1,:)))*Xe(1);
Y_xy_v_r=P_xy(2,:)+ones(size(P_xy(2,:)))*Xe(2);

%Forza
X_xy_f=sigma(2)*cos(t);
Y_xy_f=sigma(1)*sin(t);
P_xy=V*[X_xy_f; Y_xy_f; zeros(size(X_xy_f))];
X_xy_f_r=P_xy(1,:)+ones(size(P_xy(1,:)))*Xe(1);
Y_xy_f_r=P_xy(2,:)+ones(size(P_xy(2,:)))*Xe(2);

veloc_ellips(:,1) = X_xy_v_r;
veloc_ellips(:,2) = Y_xy_v_r;

force_ellips(:,1) = X_xy_f_r;
force_ellips(:,2) = Y_xy_f_r;
end

