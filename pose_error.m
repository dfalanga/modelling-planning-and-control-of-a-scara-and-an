% Input: desired pose and actual pose
% Output: pose error (position error and orientation error for phi angle around z0 axis)

function error = pose_error(Xd,Xe)

% Orientation error:
Rot_d = angle2rot(Xd(4),0,0);
Rot_e = angle2rot(Xe(4),0,0);
% Find the relative orientation between desired and actual pose
Orientation_error_matrix = transpose(Rot_e)*Rot_d;
% Find the error angle as in (2.22) 
phi_error = atan2(Orientation_error_matrix(2,1),Orientation_error_matrix(1,1));

% Position error:
% The position error is the difference between the two position vectors
position_error = Xd(1:3) - Xe(1:3);

error = [position_error phi_error];

end